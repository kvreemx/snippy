#include <iostream>
#include <vector>
#include <random>
 
int main()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::discrete_distribution<> d({40, 10, 10, 40});
    std::vector<int> freq = {0,0,0,0}; 
    for(int n=0; n<10000; ++n) {
        ++freq[d(gen)];
    }
    for(int i = 0; i<freq.size(); i++ ) {
        std::cout << i << " generated " << freq[i]<< " times\n";
    }
}
